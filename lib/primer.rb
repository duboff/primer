require 'primer/version'
require 'primer/generator'
require 'primer/table'
require 'thor'

class Primer::CLI < Thor
  default_task :print

  desc 'print N', 'print a multiplication table for first N primes'
  def print(n)
    primes = Primer::Generator.new(n: n.to_i).first_primes
    table = Primer::Table.new(array: primes).strings

    table.each { |row| puts row }
  end
end
