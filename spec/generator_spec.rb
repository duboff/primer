require 'spec_helper'
require 'prime'

describe Primer::Generator do
  let(:short_generator) { Primer::Generator.new(n: 10) }
  let(:med_generator) { Primer::Generator.new(n: 1_000_000) }

  it 'is created with a length' do
    expect(short_generator.n).to eq 10
  end

  it 'generates correct primes for a given short length' do
    expect(short_generator.first_primes).to eq Prime.first(10)
  end

  it 'generates correct primes for medium length list' do
    expect(med_generator.first_primes).to eq Prime.first(1_000_000)
  end
end
