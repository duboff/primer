require 'spec_helper'

describe Primer::Table do
  let(:array) { [1, 2, 3] }
  let(:table) { described_class.new(array: array) }

  it 'prints the right column labels' do
    expect(table.strings.first).to match(/1/)
    expect(table.strings.first).to match(/2/)
    expect(table.strings.first).to match(/3/)
  end

  it 'prints a separator' do
    expect(table.strings[1]).to match(/-/)
  end

  it 'prints multiplications' do
    expect(table.strings[2]).to match(/1/)
    expect(table.strings[2]).to match(/2/)
    expect(table.strings[2]).to match(/3/)
    expect(table.strings[4]).to match(/3/)
    expect(table.strings[4]).to match(/6/)
    expect(table.strings[4]).to match(/9/)
  end
end
