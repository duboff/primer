module Primer
  class Table
    def initialize(array:)
      @array = array
    end

    def strings
      rows.map.with_index do |row, i|
        "#{columns[i]} | #{row.join(' ')}"
      end
    end

    private

    attr_reader :array

    def multiplications
      array.map do |el|
        array.map { |e| e * el }
      end
    end

    def rows
      [formatted_array, separator] + formatted_multiplications
    end

    def separator
      Array.new(array.size, '-' * max_string_size)
    end

    def columns
      [' ' * max_string_size, '-' * max_string_size] + formatted_array
    end

    def formatted_array
      array.map { |num| "#{format % num}" }
    end

    def formatted_multiplications
      multiplications.map do |row|
        row.map { |num| "#{format % num}" }
      end
    end

    def format
      "%#{max_string_size}d"
    end

    def max_string_size
      (array.max**2).to_s.size
    end
  end
end
