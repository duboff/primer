module Primer
  class Generator
    attr_reader :n

    def initialize(n:)
      @n = n
    end

    def first_primes
      sieve.first(n)
    end

    private

    def sieve
      numbers_to_sieve.each do |num|
        next unless num
        break if num**2 > max_prime
        (num**2).step(max_prime, num) { |idx| numbers_to_sieve[idx] = nil }
      end
      numbers_to_sieve.compact
    end

    def numbers_to_sieve
      @numbers ||= Array.new(2, nil) + (2..max_prime).to_a
    end

    def max_prime
      # Rosser's theorem
      n * (Math.log(n) + Math.log(Math.log(n)))
    end
  end
end
