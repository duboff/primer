# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'primer/version'

Gem::Specification.new do |spec|
  spec.name          = "duboff-primer"
  spec.version       = Primer::VERSION
  spec.authors       = ["Mikhail Dubov"]
  spec.email         = ["mdubov@gmail.com"]
  spec.summary       = %q{A simple generator of a n x n prime multiplication table.}
  spec.description   = %q{Generates a multiplication table for the first n primes}
  spec.homepage      = "https://gitlab.com/duboff/primer"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "thor"
end
